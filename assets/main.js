var AppStream = angular.module('AppStream', ['ngRoute', 'ngDragDrop']);

AppStream
.controller('HomeController', function ($scope, $location) {

    $scope.submitName = function(name){
        $location.path( "/user/" + name );
    };

});

AppStream
.controller('UserController', function ($scope, $routeParams) {

    $scope.user = $routeParams.user;

    $scope.producstmembers = {};
    $scope.topics = {};
    $scope.responses = {};

    io.socket.join('wellinformed');
    
    $scope.getProductsMembers = function(){
        io.socket.get('/productsmember', function (productsmembers) {
            $scope.productsmembers = productsmembers;
            $scope.$apply();
        });
    }

    $scope.getTopics = function(topics){
        io.socket.get('/topic', function (topics) {
            $scope.topics = topics;
            $scope.$apply();
        });
    }

    $scope.listenForResponses = function(){
        io.socket.on('response', function(socket) {
            console.log("We're connected to someone now. Let's listen for responses from them");
            console.log(socket);
        });
    }

    $scope.getProductsMembers();
    $scope.getTopics();
    $scope.listenForResponses();

    // $scope.$on('menuboardLoaded', function(ngRepeatFinishedEvent) {
    //     $( ".droppable" ).droppable({
    //       drop: function( event, ui ) {
    //         // console.log(this.id, ui.draggable.context.id);
    //         $scope.setToMenuboard(1, this.id, ui.draggable.context.id)
    //       }
    //     });
    // });

    // $scope.setToMenuboard = function(menuboard, zone, content){

    //     // console.log($scope.zonecontainers);
    //     var zone1 =  {"title":"Zone 1","posX":0,"posY":0,"width":960,"height":1080,"content": $scope.contents[content-1] };
    //     var zone2 =  {"title":"Zone 1","posX":960,"posY":0,"width":960,"height":1080,"content": $scope.contents[content-1] };

    //     io.socket.put('/menuboard/1',
    //         {
    //             "zones":[ zone1, zone2 ]
    //         },
    //         function (resData) {
    //             console.log(resData);
    //         }
    //     );
    // }

    // $scope.onStart = function(content){
    //     console.log('yow');
    //     // io.socket.put('/menuboard/1', {"zones":[{"title":"Zone 1","posX":0,"posY":0,"width":960,"height":1080,"content":{}},{"title":"Zone 1","posX":960,"posY":0,"width":960,"height":1080,"content":{}}]}, function (resData) {
    //     //   console.log(resData);
    //     // });
    // }

    // $scope.zonecontainers = [];

    // $scope.getContents();
    // $scope.getMenuboard(1);

});

AppStream
.controller('MenuboardController', function ($scope) {
    console.log('Menuboard');
});

AppStream
.config(function ($routeProvider, $locationProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'templates/home.html',
        controller: 'HomeController'
    }).
    when('/user/:user', {
        templateUrl: 'templates/user.html',
        controller: 'UserController'
    }).
    when('/menuboard', {
        templateUrl: 'templates/menuboard.html',
        controller: 'MenuboardController'
    }).
    otherwise({
        redirectTo: '/'
    });

});